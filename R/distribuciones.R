
library(truncdist)
hist(rtrunc(n = 2000,a =  0.001, b =  0.999, spec = "norm", 0.5, 0.1))

seg <- seq(0.5,0.99, 0.01)

pdf("../Latex/Plots/multiplicador.pdf", width=6, height=6) 
plot(seg, 0.3 / (1 + exp(-25*(seg-0.8))), ylab = "",
     xlab = "Indicador Similaridad", cex.main=0.95, ylim = c(0,0.3))
legend(0.52, 0.25, legend = "r",pch = 1, col = "black", bty = "n")
dev.off()

## Normal PDF
x <- seq(-4,4,.001)
y <- dnorm(x)
plot(x,y)

## Beta PDF 

x <- seq(0,1,.001)
y <- dbeta(x,2,1)
plot(x,y, ylim = c(0,3))

x <- seq(0,1,.001)
y <- dbeta(x,3,2)
lines(x,y)

x <- seq(0,1,.001)
y <- dbeta(x,4,2)
lines(x,y)

x <- seq(0,1,.001)
y <- dbeta(x,5,2)
lines(x,y)

x <- seq(0,1,.001)
y <- dbeta(x,6,2)
lines(x,y)

x <- seq(0,1,.001)
y <- dbeta(x,8,2)
lines(x,y)


# 
x <- seq(0,1,.001)
y <- dbeta(x,2,1)
plot(x,y,type='n')
lines(x,y)
